import { createRouter, createWebHistory } from "vue-router";

import Profile from "../pages/Profile/index.vue";
import Courses from "../pages/Courses/index.vue";
import Certs from "../pages/Certs/index.vue";
import Calendar from "../pages/Calendar/index.vue";
import CourseDetailQuestions from "../pages/CourseDetail/CourseDetailQuestions.vue";
import CourseDetail from "../pages/CourseDetail/CourseDetail.vue";
import KYC from '../pages/Account/KYC.vue'
import Account from '../pages/Account/index.vue'
import Course from '../pages/Course/index.vue'
import Enrollment from '../pages/Enrollement/index.vue'
import Question from '../pages/Question/index.vue'

import Story from "../pages/Story.vue";

const routes = [
  {
    path: "/profile",
    name: "Profile",
    component: Profile,
  },
  {
    path: "/courses",
    name: "Courses",
    component: Courses,
  },
  {
    path: "/calendar",
    name: "Calendar",
    component: Calendar,
  },
  {
    path: "/account",
    name: "Account",
    component: Account,
  },
  {
    path: "/certs",
    name: "Certificates",
    component: Certs,
  },
  {
    path: "/question",
    name: "Question",
    component: Question,
  },
  {
    path: "/course",
    name: "Course",
    component: Course,
  },
  {
    path: "/course-detail",
    name: "CourseDetail",
    component: CourseDetail,
  },
  {
    path: "/course-detail-questions",
    name: "CourseDetailQuestions",
    component: CourseDetailQuestions,
  },
  {
    path: "/enrollment",
    name: "Enrollment",
    component: Enrollment,
  },
  {
    path: "/kyc",
    name: "KYC",
    component: KYC,
  },
  {
    path: "/story",
    name: "Story",
    component: Story,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
