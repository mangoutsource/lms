module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    fontFamily: {
      sans: ["DB Sathorn X"],
    },
    fontSize: {
      xs: "1.1rem",
      sm: "1.3rem",
      md: "1.4rem",
      lg: "1.6rem",
      xl: "1.8rem",
      "2xl": "1.9rem",
      "3xl": "2.1rem",
      "4xl": "2.25rem",
      "5xl": "2.4rem",
      "6xl": "2.7rem",
      "7xl": "3.0rem",
    },
    extend: {
      colors: {
        primary: {
          500: '#002F93',
          400: '#689BFA',
          300: '#E4EDFE',
          200: '#FAFCFF',
        },
        secondary: {
          500: '#49474A',
          400: '#9E9E9E',
          300: '#C4C3C8',
          200: '#FAFAFA',
        },
        semantic: {
          success: '#0F9D58',
          warning: '#FBBC05',
          danger: '#C23E5F',
          info: '#0038E5',
        }
      },
    }
  },
  plugins: [],
};
